process.env.NODE_ENV = "test";

let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../server");
let should = chai.should();

chai.use(chaiHttp);

describe("/GET topActiveUsers", () => {
  it("it should return array of users", done => {
    chai
      .request(server)
      .get("/topActiveUsers")
      .query({ page: 1 })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("array");
        if (res.body.length !== 0) {
          res.body[0].should.have.property("id");
          res.body[0].should.have.property("created_at");
          res.body[0].should.have.property("name");
          res.body[0].should.have.property("count");
          res.body[0].should.have.property("listings");
        }

        done();
      });
  });
});

describe("/GET users", () => {
    it("it should return user object", done => {
      chai
        .request(server)
        .get("/users")
        .query({ id: 1 })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an("object");
          if (res.body.length !== 0) {
            res.body.should.have.property("id");
            res.body.should.have.property("created_at");
            res.body.should.have.property("companies");
            res.body.should.have.property("createdListings");
            res.body.should.have.property("applications");
          }
  
          done();
        });
    });
  });
require("dotenv").config();
const express = require("express");
const app = (module.exports = express());
const port = process.env.PORT | 8080;
const router = require("./routes");
router(app);

if (!module.parent) { // Only listen if not called from a test (to avoid port in use error during testing)
  app.listen(port, () => {
    console.log(`App is listening to ${port}`);
  });
}

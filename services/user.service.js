const db = require("./../models");

module.exports = {
  getTopActiveUsers: async function(page) {
    const limit = 5;
    const offset = (page - 1) * limit;
    return await db.sequelize.query(
      "SELECT users.id, users.created_at, users.name, " +
        " SUM (CASE WHEN (applications.created_at > now() - interval '1 week') THEN 1 ELSE 0 END) as count," +
        " string_agg(listings.name, ', ' ORDER BY applications.created_at DESC) as listings FROM applications" +
        " RIGHT OUTER JOIN users ON applications.user_id=users.id" +
        " LEFT OUTER JOIN listings ON listings.id=applications.listing_id" +
        " GROUP BY users.id ORDER BY COUNT(*) DESC, listings ASC, users.id OFFSET :offset FETCH NEXT :limit ROWS ONLY",
      { replacements: { offset: [offset], limit: [limit] } }
    );
  },

  getUser: async function(id) {
    return await db.sequelize.query(
      "SELECT companies.id as company_id, companies.name as company_name, companies.created_at as comapny_created_at, teams.contact_user," +
        " users.id as user_id, users.name as user_name, users.created_at as user_created_at," +
        " listings.id as listing_id, listings.name as listing_name, listings.created_at as listing_created_at, listings.description as listing_description," +
        " applications.id as app_id, applications.created_at as app_created_at, applications.cover_letter as app_cover_letter," +
        " app_listings.id as app_listings_id FROM users" +
        " LEFT OUTER JOIN listings ON users.id=listings.created_by" +
        " LEFT OUTER JOIN teams ON users.id=teams.user_id" +
        " LEFT OUTER JOIN applications ON applications.user_id=users.id" +
        " LEFT OUTER JOIN listings as app_listings on applications.listing_id=app_listings.id" +
        " LEFT OUTER JOIN companies ON companies.id=teams.company_id WHERE users.id=:id",
      { replacements: { id: [id] } }
    );
  }
};

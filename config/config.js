module.exports = {
  development: {
    username: process.env.DEV_USERNAME,
    password: process.env.DEV_PASSWORD,
    database: process.env.DATABASE,
    host: process.env.DEV_DB_HOST,
    dialect: process.env.DIALECT,
    env: process.env.NODE_ENV
  },
  test: {
    username: process.env.DEV_USERNAME,
    password: process.env.DEV_PASSWORD,
    database: process.env.DATABASE,
    host: process.env.DEV_DB_HOST,
    dialect: process.env.DIALECT,
    env: process.env.NODE_ENV
  },
  production: {
    username: process.env.PROD_USERNAME,
    password: process.env.PROD_PASSWORD,
    database: process.env.DATABASE,
    host: process.env.PROD_DB_HOST,
    dialect: process.env.DIALECT,
    env: process.env.NODE_ENV
  }
};

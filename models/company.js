module.exports = (sequelize, type) => {
  const Company = sequelize.define(
    "Company",
    {
      id: {
        type: type.INTEGER,
        primaryKey: true
      },
      name: type.TEXT,
      created_at: type.DATE
    },
    {
      timestamps: false,
      underscored: true,
      tableName: "companies"
    }
  );

  Company.associate = function(models) {
    Company.belongsToMany(models.User, { through: models.Team });
  }

  return Company;
};

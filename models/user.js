module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },
      name: DataTypes.TEXT,
      created_at: DataTypes.DATE
    },
    {
      timestamps: false,
      underscored: true,
      tableName: 'users'
    }
  );

  User.associate = (models) => {
    User.belongsToMany(models.Company, { as: 'companies', through: models.Team });
    User.hasMany(models.Listing, { as: 'createdListings', foreignKey: 'created_by' });
    User.hasMany(models.Application, { as: 'applications' });
  };

  User.prototype.toJSON = function () {
    let values = Object.assign({}, this.get());
    if (values.companies[0]) {
      values.companies[0].dataValues.isContact = values.companies[0].Team.dataValues.contact_user;
      delete values.companies[0].Team.dataValues
    }
    return values;

  }

  return User;
};

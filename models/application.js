module.exports = (sequelize, type) => {
  const Application = sequelize.define(
    "Application",
    {
      id: {
        type: type.INTEGER,
        primaryKey: true
      },
      created_at: type.DATE,
      user_id: type.INTEGER,
      listing_id: type.INTEGER,
      cover_letter: type.TEXT
    },
    {
      timestamps: false,
      underscored: true,
      tableName: 'applications'
    }
);

  Application.associate = function(models) {
    Application.belongsTo(models.User);
    Application.belongsTo(models.Listing, { as: 'listing'});
  };

  return Application;
};

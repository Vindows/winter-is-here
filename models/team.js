module.exports = (sequelize, type) => {
    const Team = sequelize.define("Team", {
        user_id: {
          type: type.INTEGER, 
          primaryKey: true
        },
        company_id: {
          type: type.INTEGER, 
          primaryKey: true
        },
        contact_user: type.BOOLEAN
      },
      {
        timestamps: false,
        underscored: true,
        tableName: "teams"
      }
    );
    return Team;
  };
  
module.exports = (sequelize, type) => {
    const Listing = sequelize.define("Listing", {
        id: {
          type: type.INTEGER,
          primaryKey: true
        },
        name: type.TEXT,
        created_at: type.DATE,
        created_by: type.INTEGER,
        description: type.TEXT
      },
      {
        timestamps: false,
        underscored: true,
        tableName: "listings"
      }
    );


    Listing.associate = function(models) {
      Listing.belongsTo(models.User, { as: 'createdListings', foreignKey: 'created_by'});
      Listing.belongsTo(models.User, { as: 'listings', foreignKey: 'created_by'});
      Listing.hasMany(models.Application);
    }

    return Listing;
  };
  
# Back-end Node.js API #


## Steps to get up and running
1. Run `npm install`
2. Create a .env file in the root of the project
3. Copy/Paste contents from .env.default to .env file
4. Assign the values for the keys provided in .env
5. Run `npm start`

## With Dockerfile
1. Create a .env file in the root of the project
1. Copy/Paste contents from .env.default to .env file
2. Assign the values for the keys provided in .env
1. Run `docker build -t <tag> <path to application root>`
1. Run `docker run -p <port>:8080 <tag>`

### Notes
* I used express as I am most familiar with it at the moment, but I think a lighter server would be a better choice 
* I used Sequelize object model for /users endpoint. It seemed easier/more efficient to use SQL directly for the /topActiveUsers endpoint. Does minimum number of operations and returns necessary data in a single request.

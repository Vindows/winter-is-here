const express = require("express");
const router = express.Router();
const userService = require("./../services/user.service");

module.exports = router;

router.get("/", (req, res) => {
  res.redirect("/topActiveUsers?page=1");
});

router.get("/topActiveUsers", async (req, res) => {
  const { page } = req.query;
  let users = null;
  if (!page) {
    res.redirect("/topActiveUsers?page=1");
  }

  try {
    users = await userService.getTopActiveUsers(page);
    for (let user of users[0]) {
      if (user.listings) {
        user.listings = user.listings.split(", ");
        user.listings = user.listings.slice(0, 3);
      } else {
        user.listings = [];
      }
    }
    res.send(users[0]);
  } catch (error) {
    console.log("Error getting top active users", error);
    res.send(error.toString());
  }
});

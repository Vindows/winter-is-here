const express = require("express");
const router = express.Router();
const { User, Company, Listing, Application } = require('./../models');

module.exports = router;



router.get('/', (req, res) => {
  const { id } = req.query;
  User.findAll({ 
    where: { 
      id: id
    },
  include: [
    {
      model: Company,
      as: 'companies',
      through: {attributes: ['contact_user']}
    },
    {
      model: Listing,
      attributes: ['id', 'created_at', 'name', 'description'],
      as: 'createdListings'
    },
    {
      model: Application,
      as: 'applications',
      attributes: ['id', 'created_at', 'cover_letter'],
      include: [
        { model: Listing, 
          as: 'listing',
        attributes: ['id', 'name', 'description'] }
      ]
    }
  ]})
  .then((users) => {
    res.send(users[0]);
  })
  .catch((error) => {
    console.log("Error getting user", error);
    res.send(error.toString());
  });
  
});

// router.get("/", async (req, res) => {
//   const { id } = req.query;
//   const companyMap = {};
//   const listingMap = {};
//   const applicationMap = {};
//   let user = {};
//   const companies = [];
//   const createdListings = [];
//   const applications = [];
//   const result = await userService.getUser(id);
//   for (let row of result.rows) {
//     user = buildUserObj(row);
//     if (!companyMap[row.company_id]) {
//       companyMap[row.company_id] = row.company_id;
//       companies.push(buildCompanyObj(row));
//     }
//     if (!listingMap[row.listing_id]) {
//       listingMap[row.listing_id] = row.listing_id;
//       createdListings.push(buildCreatedListingsObj(row));
//     }
//     if (!applicationMap[row.app_id]) {
//       applicationMap[row.app_id] = row.app_id;
//       applications.push(buildApplicationObj(row));
//     }

//     user.companies = companies;
//     user.createdListings = createdListings;
//     user.applications = applications;
//   }
//   res.send(user);
// });

function buildApplicationObj(row) {
  let application = {};
  if (row.app_id) {
    application.id = row.app_id;
    application.created_at = row.app_created_at;
    application.listing = {
      id: row.app_listings_id
    };
    application.cover_letter = row.app_cover_letter;
    return application;
  }
  return;
}

function buildCompanyObj(row) {
  let company = {};
  company.id = row.company_id;
  company.created_at = row.company_created_at;
  company.name = row.company_name;
  company.isContact = row.contact_user;
  return company;
}

function buildCreatedListingsObj(row) {
  let listing = {};
  listing.id = row.listing_id;
  listing.name = row.listing_name;
  listing.created_at = row.listing_created_at;
  listing.description = row.listing_description;
  return listing;
}

function buildUserObj(row) {
  let user = {};
  user.id = row.user_id;
  user.name = row.user_name;
  user.created_at = row.user_created_at;

  return user;
}
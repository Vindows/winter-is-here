const express = require('express');
const root = require('./root');
const users = require('./users')

module.exports = (app) => {
    app.use('/', root);
    app.use('/users', users);
}
